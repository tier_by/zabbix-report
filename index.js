const pug = require('pug');
const fetch = require('node-fetch');
const Mailgun = require('mailgun-js');

const mailgun = new Mailgun({
    apiKey: 'key-a73fcfef31c908ec6698b781e24f8d03',
    domain: 'sandbox303df068819b4eaaac186d1b28cd831c.mailgun.org'
});

const ZABBIX_URL = 'https://jsonplaceholder.typicode.com/users';

fetch(ZABBIX_URL)
    .then(res => res.text())
    .then(body => renderEmailTemplate(body))
    .then(emailContent => sendEmail('vladimirsemashko@mail.ru', 'ZABBIX TEST', emailContent));

//////////////////////////////////////////////////

function renderEmailTemplate(users) {
    return pug.renderFile('email-template.pug', {
        users: JSON.parse(users)
    });
}

function sendEmail(to, subject, htmlBody) {
    let data = {
        from: 'Zabbix Reporter <postmaster@sandbox303df068819b4eaaac186d1b28cd831c.mailgun.org>',
        to: to,
        subject: subject,
        html: htmlBody
    };
    mailgun.messages().send(data, (err, body) => {
        if (err) {
            return console.log("got an error: ", err);
        }
        console.log(`Message sent. id = ${body.id}; \nmessage = ${body.message}`);
    });
}
